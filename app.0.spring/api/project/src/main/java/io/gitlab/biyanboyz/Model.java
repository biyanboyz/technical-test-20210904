package io.gitlab.biyanboyz;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "md_banks")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class Model{
  @Id
  @Column(name = "id")
  private String id;
  @Column(name = "bank_name")
  private String bank_name;
  @Column(name = "email")
  private String email;
  public Model(String id, String bank_name, String email){
    this.id = id;
    this.bank_name = bank_name;
    this.email = email;
  }
  public String getId(){
    return this.id;
  }
  public String getBankName(){
    return this.bank_name;
  }
  public String getEmail(){
    return this.email;
  }
  public void setId(String val){
    this.id = val;
  }
  public void setBankName(String val){
    this.bank_name = val;
  }
  public void setEmail(String val){
    this.email = val;
  }
}