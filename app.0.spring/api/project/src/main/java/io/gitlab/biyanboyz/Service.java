package io.gitlab.biyanboyz;

import org.springframework.dao.DataAccessException;
import java.util.Collection;

public interface Service{
  Collection<io.gitlab.biyanboyz.Model> read() throws DataAccessException;
  void create(io.gitlab.biyanboyz.Model data) throws DataAccessException;
  void update(String id, io.gitlab.biyanboyz.Model data) throws DataAccessException;
  void delete(String id) throws DataAccessException;
}