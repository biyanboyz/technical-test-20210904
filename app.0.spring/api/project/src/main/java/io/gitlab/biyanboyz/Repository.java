package io.gitlab.biyanboyz;

import org.springframework.dao.DataAccessException;
import javax.persistence.EntityManager;
import java.util.Collection;

public interface Repository{
  Collection<io.gitlab.biyanboyz.Model> read() throws DataAccessException;
  void create(io.gitlab.biyanboyz.Model data) throws DataAccessException;
  void update(String id, io.gitlab.biyanboyz.Model data) throws DataAccessException;
  void delete(String id) throws DataAccessException;
}