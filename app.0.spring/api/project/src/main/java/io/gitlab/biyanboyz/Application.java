package io.gitlab.biyanboyz;

import java.util.Collection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootApplication
@RestController
public class Application {

  @Autowired
  private Service service;

  @RequestMapping(value = "/md_banks", method=org.springframework.web.bind.annotation.RequestMethod.GET)
  public Collection<Model> mdbanks_get() {
    return service.read();
  }
  @RequestMapping(value = "/md_banks", method=org.springframework.web.bind.annotation.RequestMethod.POST)
  public void mdbanks_post(String id, String name, String email) {
    service.create(new Model(id, name, email));
  }
  @RequestMapping(value = "/md_banks", method=org.springframework.web.bind.annotation.RequestMethod.PUT)
  public void mdbanks_put(String old_id, String id, String name, String email) {
    service.update(old_id, new Model(id, name, email));
  }
  @RequestMapping(value = "/md_banks", method=org.springframework.web.bind.annotation.RequestMethod.DELETE)
  public void mdbanks_delete(String id, String name, String email) {
    service.delete(id);
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
