package io.gitlab.biyanboyz;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.PersistenceContext;

@Repository
@Transactional(Transactional.TxType.REQUIRES_NEW)
public class RepositoryImpl implements io.gitlab.biyanboyz.Repository{
  @PersistenceContext
  private EntityManager em;

  @Override
  @SuppressWarnings("unchecked")
  public Collection<io.gitlab.biyanboyz.Model> read() throws DataAccessException{
    return this.em.createNativeQuery("SELECT * FROM md_banks").getResultList();
  }
  @Override
  public void create(io.gitlab.biyanboyz.Model data) throws DataAccessException{
    Query q = this.em.createNativeQuery("INSERT INTO md_banks VALUES (:id, :name, :email)");
    q.setParameter("id", data.getId());
    q.setParameter("name", data.getBankName());
    q.setParameter("email", data.getEmail());
    q.executeUpdate();
  }
  @Override
  public void update(String id, io.gitlab.biyanboyz.Model data) throws DataAccessException{
    Query q = this.em.createNativeQuery("UPDATE md_banks SET id = :id, bank_name = :name, email = :email WHERE id = :old_id");
    q.setParameter("old_id", id);
    q.setParameter("id", data.getId());
    q.setParameter("name", data.getBankName());
    q.setParameter("email", data.getEmail());
    q.executeUpdate();
  }
  @Override
  public void delete(String id) throws DataAccessException{
    Query q = this.em.createNativeQuery("DELETE FROM md_banks WHERE id=:id");
    q.setParameter("id", id);
    q.executeUpdate();
  }
}