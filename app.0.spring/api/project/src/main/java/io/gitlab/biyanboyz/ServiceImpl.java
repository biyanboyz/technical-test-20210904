package io.gitlab.biyanboyz;

import org.springframework.dao.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class ServiceImpl implements io.gitlab.biyanboyz.Service{
  private Repository repository;
  @Autowired
  public ServiceImpl(Repository repository) throws DataAccessException{
    this.repository = repository;
  }
  @Override
  public Collection<io.gitlab.biyanboyz.Model> read() throws DataAccessException{
    return this.repository.read();
  }
  @Override
  public void create(io.gitlab.biyanboyz.Model data) throws DataAccessException{
    this.repository.create(data);
  }
  @Override
  public void update(String id, io.gitlab.biyanboyz.Model data) throws DataAccessException{
    this.repository.update(id, data);
  }
  @Override
  public void delete(String id) throws DataAccessException{
    this.repository.delete(id);
  }
}