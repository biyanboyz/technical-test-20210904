# Technical Test 2021-09-05
## By: Muhamad Biyan Abdulah ; For: Juke Solusi Teknologi

- Docker Compose MD_Banks Spring
  - ✔️ Spring Boot
  - ✔️ Docker
  - ✔️ Docker Image available at Docker Hub
  - ✔️ Swagger YAML (Spring)
- Docker Compose MD_Banks Typescript
  - ✔️ Typescript
  - ✔️ Docker
  - ✔️ Docker Image available at Docker Hub
  - ✔️ Swagger YAML
  - ✔️ Asynchronous
  - ✔️ VS Code Configuration helps Typescript Imports and Webpack Tasks
  - ✔️ (Partial) Hexagonal Architecture
  - Dependencies used: Express.Js, node-postgres
- Docker Compose MD_Banks Rust
  - ✔️ Rust
  - ✔️ Docker
  - ✔️ Docker Image available at Docker Hub
  - ✔️ Swagger YAML
  - ✔️ Asynchronous
  - Dependencies used: Actix, Serde, postgre
