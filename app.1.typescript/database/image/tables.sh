#!/bin/bash
psql -v ON_ERROR_STOP=1 --username postgres --dbname masterdata <<-EOSQL
  CREATE TABLE md_banks (id VARCHAR(3), bank_name VARCHAR(100), email VARCHAR(100));
  INSERT INTO md_banks VALUES ('A', 'Bank A', 'contact@banka.com');
  INSERT INTO md_banks VALUES ('B', 'Bank B', 'contact@bankb.com');
  INSERT INTO md_banks VALUES ('C', 'Bank C', 'contact@bankc.com');
EOSQL