import express from 'express';
import { Server } from 'http';
import pg from 'pg';
import { PostgreImplmnt } from './core/postgre_implmnt';

const app = express();
const port = 8083;
const server = PostgreImplmnt.server(new pg.Pool({
  user: 'postgres',
  host: 'database',
  database: 'masterdata',
  password: 'admin',
  port: 5432,
}));

app.get('/md_banks', (_request, response) => {
  server.read()
  .then(result => response.send(result))
  .catch(result => response.send(result))
});
app.post('/md_banks', (request, response) => {
  server.create(request.query)
  .then(result => response.send(result))
  .catch(result => response.send(result))
});
app.put('/md_banks', (request, response) => {
  server.update(request.query.old_id, {id: request.query.id, email: request.query.email, name: request.query.name})
  .then(result => response.send(result))
  .catch(result => response.send(result))
});
app.delete('/md_banks', (request, response) => {
  server.delete(request.query.id)
  .then(result => response.send(result))
  .catch(result => response.send(result))
});
app.listen(port, () => {
  console.log(`Ready at port ${port}`)
});