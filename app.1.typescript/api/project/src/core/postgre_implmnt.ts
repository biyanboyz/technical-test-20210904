import { Abstracts } from "./abstracts";

export namespace PostgreImplmnt{
  export const server: (postgrePool)=>Abstracts.Server<any> = (postgrePool)=>({
    read: ()=>(
      postgrePool
      .query({
        text: 'SELECT * FROM md_banks',
      })
      .then(result => ({
        success: true,
        data: result.rows,
        err: null,
      }))
      .catch(err =>{
        console.log(err);
        return ({
          success: false,
          data: [],
          error: err,
        })
      })
    ),
    create: (val)=>(
      postgrePool
      .query({
        text: 'INSERT INTO md_banks VALUES ($1, $2, $3)',
        values: [
          val.id,
          val.name,
          val.email,
        ]
      })
      .then(result => ({
        success: true,
        err: null,
      }))
      .catch(err => ({
        success: false,
        error: err,
      }))
    ),
    update: (id, val)=>(
      postgrePool
      .query({
        text: 'UPDATE md_banks SET id=$2, bank_name=$3, email=$4 WHERE id=$1',
        values: [
          id,
          val.id,
          val.name,
          val.email,
        ]
      })
      .then(result => ({
        success: true,
        err: null,
      }))
      .catch(err =>({
        success: false,
        error: err,
      }))
    ),
    delete: (id: String)=>(
      postgrePool
      .query({
        text: 'DELETE FROM md_banks WHERE id=$1',
        values: [
          id,
        ]
      })
      .then(result => ({
        success: true,
        err: null,
      }))
      .catch(err => ({
        success: false,
        error: err,
      }))
    )
  })
}