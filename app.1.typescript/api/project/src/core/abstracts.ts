export namespace Abstracts{
  export type Item = {
    id: String,
    name: String,
    email: String
  };
  export type Server<TError> = {
    read: ()=>Promise<{success: boolean, data: Item[], error: TError}>,
    create: (val: Item)=>Promise<{success: boolean, error: TError}>,
    update: (id: String, val: Item)=>Promise<{success: boolean, error: TError}>,
    delete: (id: String)=>Promise<{success: boolean, error: TError}>
  }
}