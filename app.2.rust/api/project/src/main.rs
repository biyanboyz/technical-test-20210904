use actix_web::{web, App, HttpRequest, HttpServer};

pub mod core{
  pub mod abstracts{
    #[derive(serde::Deserialize, serde::Serialize)]
    pub struct Item{
      pub id: String,
      pub name: String,
      pub email: String,
    }
    pub trait Core<TError>{
      fn read(&mut self)->Result<Vec<Item>, TError>;
      fn create(&mut self, val: Item)->Result<(), TError>;
      fn update(&mut self, id: String, val: Item)->Result<(), TError>;
      fn delete(&mut self, id: String)->Result<(), TError>;
    }
  }
  pub mod implmnts{
    pub mod postgres{
      use crate::core::abstracts;
      pub static DEFAULT_HOST : &str = "database";
      pub static DEFAULT_DATABASE : &str = "masterdata";
      pub static DEFAULT_USERNAME : &str = "postgres";
      pub static DEFAULT_PASSWORD : &str = "admin";
      pub struct Core(postgres::Client);
      impl Core{
        pub fn new(host: &str, database: &str, username: &str, password: &str)->Core{
          Core(postgres::Client::connect(format!("host={} dbname={} user={} password={}", host, database, username, password).as_str(), postgres::NoTls).unwrap())
        }
      }
      impl abstracts::Core<()> for Core{
        fn read(&mut self)->Result<Vec<abstracts::Item>, ()>{
          self.0
          .query(
            "SELECT * FROM md_banks",
            &[]
          )
          .map_err(|_|{ dbg!(); () })
          .and_then(|v| Ok(
            v.iter().map(|v|
              abstracts::Item{
                id: v.get("id"),
                name: v.get("bank_name"),
                email: v.get("email")
              }
            ).collect::<Vec::<_>>()
          ))
        }
        fn create(&mut self, val: abstracts::Item)->Result<(), ()>{
          self.0
          .query(
            "INSERT INTO md_banks VALUES ($1, $2, $3)",
            &[
              &val.id,
              &val.name,
              &val.email
            ]
          )
          .map_err(|_|{ dbg!(); () })
          .and_then(|_| Ok(()))
        }
        fn update(&mut self, id: String, val: abstracts::Item)->Result<(), ()>{
          self.0
          .query(
            "UPDATE md_banks SET id=$2, bank_name=$3, email=$4 WHERE id=$1",
            &[
              &id,
              &val.id,
              &val.name,
              &val.email
            ]
          )
          .map_err(|_|{ dbg!(); () })
          .and_then(|_| Ok(()))
        }
        fn delete(&mut self, id: String)->Result<(), ()>{
          self.0
          .query(
            "DELETE FROM md_banks WHERE id=$1",
            &[&id]
          )
          .map_err(|_|{ dbg!(); () })
          .and_then(|_| Ok(()))
        }
      }
    }
  }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
  // static immoved : String = String::from("Abcd");
  HttpServer::new(|| {
    App::new()
    .route("/md_banks", web::get().to({
      async fn ret(_: HttpRequest) -> actix_web::HttpResponse{
        use crate::core::{abstracts, implmnts::postgres};
        #[derive(serde::Serialize)]
        struct Output{
          pub success: bool,
          pub data: Vec<abstracts::Item>,
          pub error: Option<()>,
        }
        let mut ret = postgres::Core::new(
          postgres::DEFAULT_HOST,
          postgres::DEFAULT_DATABASE,
          postgres::DEFAULT_USERNAME,
          postgres::DEFAULT_PASSWORD,
        );
        let ret = abstracts::Core::read(&mut ret);
        let ret = match ret{
          Ok(x)=>serde_json::to_string(&Output{success: true, data: x, error: None}).unwrap(),
          _=>serde_json::to_string(&Output{success: false, data: vec![], error: Some(())}).unwrap()
        };
        let ret = {
          actix_web::HttpResponse::Ok()
          .content_type("application/json")
          .body(ret)
        };
        ret
      }
      ret
    }))
    .route("/md_banks", web::post().to({
      async fn ret(req: HttpRequest) -> actix_web::HttpResponse{
        use crate::core::{abstracts, implmnts::postgres};
        #[derive(serde::Serialize)]
        struct Output{
          pub success: bool,
          pub error: Option<()>,
        }
        let mut ret = postgres::Core::new(
          postgres::DEFAULT_HOST,
          postgres::DEFAULT_DATABASE,
          postgres::DEFAULT_USERNAME,
          postgres::DEFAULT_PASSWORD,
        );
        let ret = abstracts::Core::create(&mut ret, {
          actix_web::web::Query::<abstracts::Item>::from_query(req.query_string()).unwrap().0
        });
        let ret = match ret{
          Ok(_)=>serde_json::to_string(&Output{success: true, error: None}).unwrap(),
          _=>serde_json::to_string(&Output{success: false, error: Some(())}).unwrap()
        };
        let ret = {
          actix_web::HttpResponse::Ok()
          .content_type("application/json")
          .body(ret)
        };
        ret
      }
      ret
    }))
    .route("/md_banks", web::put().to({
      async fn ret(req: HttpRequest) -> actix_web::HttpResponse{
        use crate::core::{abstracts, implmnts::postgres};
        #[derive(serde::Deserialize)]
        struct Input{
          pub old_id: String,
          pub id: String,
          pub name: String,
          pub email: String,
        }
        #[derive(serde::Serialize)]
        struct Output{
          pub success: bool,
          pub error: Option<()>,
        }
        let mut ret = postgres::Core::new(
          postgres::DEFAULT_HOST,
          postgres::DEFAULT_DATABASE,
          postgres::DEFAULT_USERNAME,
          postgres::DEFAULT_PASSWORD,
        );
        let query = actix_web::web::Query::<Input>::from_query(req.query_string()).unwrap().0;
        let ret = abstracts::Core::update(&mut ret, query.old_id, abstracts::Item{
          id: query.id,
          name: query.name,
          email: query.email,
        });
        let ret = match ret{
          Ok(_)=>serde_json::to_string(&Output{success: true, error: None}).unwrap(),
          _=>serde_json::to_string(&Output{success: false, error: Some(())}).unwrap()
        };
        let ret = {
          actix_web::HttpResponse::Ok()
          .content_type("application/json")
          .body(ret)
        };
        ret
      }
      ret
    }))
    .route("/md_banks", web::delete().to({
      async fn ret(req: HttpRequest) -> actix_web::HttpResponse{
        use crate::core::{abstracts, implmnts::postgres};
        #[derive(serde::Deserialize)]
        struct Input{
          pub id: String,
        }
        #[derive(serde::Serialize)]
        struct Output{
          pub success: bool,
          pub error: Option<()>,
        }
        let mut ret = postgres::Core::new(
          postgres::DEFAULT_HOST,
          postgres::DEFAULT_DATABASE,
          postgres::DEFAULT_USERNAME,
          postgres::DEFAULT_PASSWORD,
        );
        let query = actix_web::web::Query::<Input>::from_query(req.query_string()).unwrap().0;
        let ret = abstracts::Core::delete(&mut ret, query.id);
        let ret = match ret{
          Ok(_)=>serde_json::to_string(&Output{success: true, error: None}).unwrap(),
          _=>serde_json::to_string(&Output{success: false, error: Some(())}).unwrap()
        };
        let ret = {
          actix_web::HttpResponse::Ok()
          .content_type("application/json")
          .body(ret)
        };
        ret
      }
      ret
    }))
  })
  .bind(("0.0.0.0", 8083))?
  .run()
  .await

  // use std::cell::Cell;
  // use actix_web::{web, App, HttpResponse, Responder};

  // struct MyData {
  //   counter: Cell<usize>,
  // }

  // async fn index(data: web::Data<MyData>) -> impl Responder {
  //     data.counter.set(data.counter.get() + 1);
  //     HttpResponse::Ok()
  // }

  // let app = App::new()
  //     .data(MyData{ counter: Cell::new(0) })
  //     .service(
  //         web::resource("/index.html").route(
  //             web::get().to(index)));

  // HttpServer::new(|| app)
  // .bind(("0.0.0.0", 8083))?
  // .run()
  // .await
}